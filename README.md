# Fork of LibVTE #

## Description (English Version) ##

This is an improved version of the LibVTE code, with excellent changes in the library where it corrects all display problems and more.
This code is restricted to the public and is priced at more than $100,000 EUR. If you want me to release the code paying it like donations to reach the goal, contact me.

## Descripcion ##

Esto es una version mejorada del codigo LibVTE, con excelentes cambios en la libreria donde corrige todos los problemas de visualizacion y demas.
Dicho codigo esta restringido al publico y se cotiza a mas de 100.000$ EUR. Si deseas que libere el codigo pagando donaciones hasta alcanzar la suma, contactame.
